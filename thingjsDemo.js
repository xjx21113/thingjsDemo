var app = new THING.App({
    // 引用场景
    url: '/api/scene/20210616095248956300069',  // 场景地址
    skyBox: 'BlueSky', // 天空盒
	resourceLibraryUrl: "./" 
});

var car;
var line;
var position;

app.on('load', function () {
    car = app.query('car01')[0];
    position = car.position;
    app.camera.xAngleLimitRange = [-90, 90];  // 设置摄像机俯仰角度范围[最小值, 最大值]
    app.camera.yAngleLimitRange = [-360, 360];  // 设置摄像机水平角度范围[最小值, 最大值]
    app.camera.enableRotate = false;
    app.camera.enablePan = false;  // 禁用平移
    app.camera.enableZoom = false;  // 禁用缩放
    movePath()

});
/**
 * 车辆对象沿路径移动
 */
function movePath() {
    // reset();
  
    var path = [[0, 0.1, 0], [20, 0.1, 0], [20, 0.1, 10], [0, 0.1, 10]];
    // 摄像机飞行到某位置
    app.camera.flyTo({
        'position': [36.013, 42.67799999999998, 61.72399999999999],
        'target': [1.646, 7.891, 4.445],
        'time': 1000,
        'complete': function () {
            car.movePath({
                orientToPath: true, // 物体移动时沿向路径方向
                path: path, // 路径坐标点数组
                time: 5 * 1000, // 路径总时间 毫秒
                delayTime: 1000, // 延时 1s 执行
                lerpType: null, // 插值类型（默认为线性插值）此处设置为不插值
                  loopType: THING.LoopType.PingPong
            });
        }
    });



}
